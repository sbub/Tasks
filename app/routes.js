import React from 'react';

import { StackNavigator } from 'react-navigation';
import { enhance } from 'react-navigation-addons';

import TaskList from './components/TaskList';
import EditTask from './components/EditTask';

const Routes = enhance(StackNavigator)({
  TaskList: {
    screen: TaskList,
  },
  EditTask: {
    screen: EditTask,
  }
}, {
  initialRouteName: 'TaskList',
})

export default Routes;
