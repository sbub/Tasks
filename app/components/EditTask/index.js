import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, View, DatePickerIOS, Button, Switch, TextInput } from 'react-native';

import moment from 'moment';

import styles from './styles';

import ExpandableCell from '../ExpandableCell';


export default class EditTask extends Component {

  constructor(props) {
    super(props);
    const stateParams = this.props.navigation.state.params;

    this.state = {
      completed: stateParams.completed,
      date: new Date(stateParams.due),
      expanded: false,
      text: stateParams.text,
      formattedDate: stateParams.formattedDate
    }
  }

  componentWillMount() {
    this.props.navigation.setOptions({
      title: 'EditTask',
      headerRight: (
        <View style={{flexDirection: 'row'}}>
                        <Button
                          onPress={() => this._onPressSave()}
                          title={'Save'}>
                        </Button>
                      </View>
      )
    });
  }

  render() {
    const noDueDateTitle = 'Set Reminder';
    const dueDateSetTile = 'Due On: ' + this.state.formattedDate
                            || this.props.navigation.state.params.formattedDate;
    return(
      <View style={styles.editTaskContainer}>
        <View>
          <TextInput
            autoCorrect={false}
            onChangeText={(value) => this._onChangeTextInputValue(value)}
            returnKeyType={'done'}
            style={styles.textInput}
            value={this.state.text}
          />
        </View>
        <View style={ [styles.expandableCellContainer, { maxHeight: this.state.expanded ? this.state.datePickerHeight : 40 }]}>
          <ExpandableCell
            title={dueDateSetTile}
            expanded={this.state.expanded}
            onPress={()=>this._onExpand()}>
            <DatePickerIOS
              date={this.state.date}
              onDateChange={(date) => this._onDateChange(date)}
              onLayout={(event) => this._getDatePickerHeight(event)}>
            </DatePickerIOS>
          </ExpandableCell>
        </View>
        <View style={styles.switchContainer}>
          <Text style={styles.switchText}>Completed</Text>
          <Switch
            onValueChange={(value) => this._onSwitchToggle(value) }
            value={ this.state.completed }
          />
        </View>
        <View style={styles.clearDateButtonContainer}>
          <Button
            color={'#B44743'}
            disabled={ this.state.dateSelected ? false : true }
            onPress={ () => this._clearDate() }
            title={'Clear Date'}>
          </Button>
        </View>
      </View>
    )
  }

  _onPressSave = () => {
    this.props.navigation.state.params.onRightButtonPress();
    setTimeout(() => {
      this.props.navigation.goBack();
    }, 1000)
  }

  _formatDate = (date) => {
    return moment(date).format('lll');
  }

  _onDateChange = (date) => {
    let formattedDate = this._formatDate(date);
    this.setState({
      date,
      dateSelected: true,
      formattedDate
    })
    this.props.navigation.state.params.changeTaskDueDate(date, formattedDate);
  }

  _getDatePickerHeight = (event) => {
    this.setState({
      datePickerHeight: event.nativeEvent.layout.width
    });
  }

  _clearDate = () => {
    this.setState({
      dateSelected: false
    })
    this.props.navigation.state.params.clearTaskDueDate();
  }

  _onExpand = () => {
    console.log("** changin on expand");
    this.setState({
      expanded: !this.state.expanded
    })
  }

  _onChangeTextInputValue = (text) => {
    this.setState({
      text
    })
    this.props.navigation.state.params.changeTaskName(text);
  }

  _onSwitchToggle = (completed) => {
    this.setState({
      completed
    })
    this.props.navigation.state.params.changeTaskCompletionStatus(completed)
  }
}
