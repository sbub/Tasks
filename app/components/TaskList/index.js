import React, { Component } from 'react';
import { ListView, Text, View, TextInput, AsyncStorage, Button } from 'react-native';

import styles from './styles';

import TaskListCell from "../TaskListCell";
import EditTask from "../EditTask";

export default class TaskList extends Component {

  static navigationOptions = {
    title: 'TaskList',
  }

  constructor(props) {
    super(props);
    this.state = {
      ds: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}),
      listOfTasks: [],
      text: '',
      currentEditedTaskObject: undefined
    }
  }

  async componentDidMount() {
    this._updateList();
  }

  render() {
    const dataSource = this.state.ds.cloneWithRows(this.state.listOfTasks);
    return(
      <View style={styles.container}>
        <TextInput
          autoCorrect={ false }
          onChangeText={(text) => this._changeTextInputValue(text)}
          onSubmitEditing={() => this._addTask()}
          returnKeyType={'done'}
          style={styles.textInput}
          value={this.state.text}
        />
        <ListView
          style={styles.tasklist}
          dataSource={dataSource}
          automaticallyAdjustContentInsets={false}
          style={styles.listView}
          enableEmptySections={true}
          renderRow={(rowData, sectionID, rowId)=>this._renderRowData(rowData, rowId)}
        />
      </View>
    )
  }

  async _updateList () {
    let response = await AsyncStorage.getItem('listOfTasks');
    let listOfTasks = await JSON.parse(response) || [];

    this.setState({listOfTasks});
    this._changeTextInputValue('');
  }

  async _addTask () {

    const singleTask = {
      completed: false,
      text: this.state.text
    }

    const listOfTasks = [...this.state.listOfTasks, singleTask];

    //writing to the AsyncStorage
    await AsyncStorage.setItem('listOfTasks', JSON.stringify(listOfTasks));

    //getting updated listOfTasks from AsyncStorage and updating state listOfTasks
    this._updateList();
  }

  _changeTextInputValue = (text) => {
    this.setState({
      text
    });
  }

  _renderRowData = (rowData, rowId) => {
    return (
      <TaskListCell
        completed={rowData.completed}
        formattedDate={rowData.formattedDate}
        id={rowId}
        onPress={() => this._completeTask(rowId)}
        onLongPress={(rowData) => this._editTask(rowId, rowData)}
        text={rowData.text}
      />
    )
  }

  _completeTask (rowId) {

    const singleUpdatedTask = {
      ...this.state.listOfTasks[rowId],
      completed: !this.state.listOfTasks[rowId].completed
    };

    //
    this._saveAndUpdateSelectedTask(singleUpdatedTask, rowId);

    // TODO: to be deleted once i know how to simulate long press
    setTimeout(() => {
      this._editTask(rowId, singleUpdatedTask);
    }, 1000)

  }

  async _saveAndUpdateSelectedTask (newTaskObj, rowId) {
    const listOfTasks = this.state.listOfTasks.slice();
    listOfTasks[rowId] = newTaskObj;
    await AsyncStorage.setItem('listOfTasks', JSON.stringify(listOfTasks));
    this._updateList();
  }



  _editTask = (rowId, rowData) => {
    this.setState({
      currentEditedTaskObject: rowData
    })
    this.props.navigation.navigate('EditTask', {
      completed: rowData.completed,
      due: rowData.due,
      formattedDate: rowData.formattedDate,
      text: rowData.text,
      onRightButtonPress: () => this._saveCurrentEditedTask(rowId),
      changeTaskCompletionStatus: (status) => this._updateCurrentEditedTaskObject('completed', status),
      changeTaskDueDate: (date, formattedDate) => this._updateCurrentEditedTaskDueDate(date, formattedDate),
      changeTaskName: (name) => this._updateCurrentEditedTaskObject('text', name),
      clearTaskDueDate: () => this._updateCurrentEditedTaskDueDate(undefined, undefined),
    })

  }

  _saveCurrentEditedTask = (rowID) => {
    this._saveAndUpdateSelectedTask(this.state.currentEditedTaskObject, rowID);
  }

  _updateCurrentEditedTaskDueDate = (date, formattedDate) => {
    this._updateCurrentEditedTaskObject('due', date);
    this._updateCurrentEditedTaskObject('formattedDate', formattedDate);
  }

  _updateCurrentEditedTaskObject = (key, value) => {
    let newTaskObj = Object.assign({}, this.state.currentEditedTaskObject);
    newTaskObj[key] = value;
    this.setState({currentEditedTaskObject: newTaskObj})
  }
}
