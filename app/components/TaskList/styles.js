import { StyleSheet } from 'react-native';
const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderColor: 'orange',
    borderWidth: 1
  },
  textInput: {
    width: 200,
    marginTop: 20,
    borderColor: 'blue',
    borderWidth: 1
  },
  tasklist: {
    width: 300,
    marginTop: 20,
    borderColor: 'green',
    borderWidth: 1
  }
});

export default styles;
