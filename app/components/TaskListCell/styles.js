import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  tasksListCellContainer: {
    flex: 1
  },
  tasksListCellTextRow: {
    flex: 1
  },
  taskNameText: {
    fontSize: 20
  },
  dueDateText: {
    color: 'red',
    flex: 1,
    fontSize: 12,
    paddingTop: 0,
    textAlign: 'right'
  }
})

export default styles;
