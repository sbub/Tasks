import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableHighlight } from 'react-native';

import styles from './styles';

export default class TaskListCell extends Component {

  static propTypes = {
    completed: PropTypes.bool.isRequired,
    id: PropTypes.string.isRequired,
    onLongPress: PropTypes.func.isRequired,
    onPress: PropTypes.func.isRequired,
    text: PropTypes.string.isRequired,
    formattedDate: PropTypes.string
  }

  constructor(props) {
    super(props);
  }

  render() {
    const isCompleted = this.props.completed ? 'line-through' : 'none';
    // const textStyle = {
    //   fontSize: 20,
    //   textDecorationLine: isCompleted
    // }
    return(
      <View style={styles.tasksListCellContainer}>
        <TouchableHighlight
          onPress={() => this.props.onPress(this.props.id)}
          underlayColor={'#D5DBDE'}>
          <View style={styles.tasksListCellTextRow}>
            <Text style={[styles.taskNameText, { textDecorationLine: isCompleted }]}>
              {this.props.text}
            </Text>
            <Text style={styles.dueDateText}>
              {this._getDueDate()}
            </Text>
          </View>
        </TouchableHighlight>
      </View>
    )
  }

  _getDueDate = () => {
    console.log("** FORMATTED DATE", this.props.formattedDate);
    if(this.props.formattedDate && !this.props.completed) {
      return 'Due' +  this.props.formattedDate;
    }
    return '';
  }
}
